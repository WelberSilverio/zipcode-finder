package com.zipcode.finder.business;

import com.zipcode.finder.dto.Address;

public interface FinderBusiness {
  Address findAddress(final String zipcode);
}
