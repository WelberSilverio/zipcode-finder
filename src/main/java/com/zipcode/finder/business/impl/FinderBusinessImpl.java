package com.zipcode.finder.business.impl;

import com.zipcode.finder.business.FinderBusiness;
import com.zipcode.finder.dto.Address;
import com.zipcode.finder.exception.AddressNotFoundException;
import com.zipcode.finder.mock.ListAddressMock;
import com.zipcode.finder.util.Constants;
import java.util.Objects;
import org.springframework.stereotype.Service;

@Service
public class FinderBusinessImpl implements FinderBusiness {

  @Override
  public Address findAddress(final String zipcode) {
    var zipcodeSearched = new StringBuilder(zipcode);
    Address address;
    int i = Constants.ZIPCODE_LENGTH;
    do {
      address = ListAddressMock.getListAddress().stream()
                    .filter(x -> x.getZipcode().equals(zipcodeSearched.toString()))
                    .findFirst()
                    .orElse(null);
      i--;
      zipcodeSearched.setCharAt(i,'0');
    } while (i >= 1 && Objects.isNull(address));

    if (Objects.isNull(address)) {
      throw new AddressNotFoundException("Zipcode not match address");
    }

    return address;
  }

}
