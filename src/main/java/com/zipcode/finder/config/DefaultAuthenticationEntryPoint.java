package com.zipcode.finder.config;

import static java.text.MessageFormat.format;

import com.google.gson.Gson;
import com.zipcode.finder.dto.ErrorMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

public class DefaultAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Override
  public void commence(HttpServletRequest request,
                       HttpServletResponse response,
                       AuthenticationException e) throws IOException {
    final ErrorMessage errorMessages =
        ErrorMessage.builder()
            .developerMessage(format("Unauthorized - {0}", e.getMessage()))
            .userMessage("You are not authorized to perform this operation")
            .errorCode(30001)
            .build();

    response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    response.setStatus(HttpStatus.UNAUTHORIZED.value());
    response.getWriter().println(new Gson().toJson(Collections.singleton(errorMessages)));
  }

}

