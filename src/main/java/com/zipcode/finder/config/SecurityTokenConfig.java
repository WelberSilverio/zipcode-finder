package com.zipcode.finder.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Profile("!test")
@Configuration
@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {

  @Value("${jwt.secretKey}")
  private String jwtSecretKey;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        .exceptionHandling()
        .authenticationEntryPoint(new DefaultAuthenticationEntryPoint())
        .and()
        .authorizeRequests()
        .antMatchers("/actuator/health", "/health/ping", "/swagger-resources/**",
            "/v2/api-docs", "/swagger-ui.html", "/webjars/**")
        .permitAll()
        .anyRequest().authenticated();
    http.addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtSecretKey));
  }

}