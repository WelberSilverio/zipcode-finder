package com.zipcode.finder.config;

import com.zipcode.finder.dto.ErrorMessage;
import com.zipcode.finder.exception.AddressNotFoundException;
import com.zipcode.finder.exception.InvalidZipcodeException;

import java.util.Collections;
import java.util.List;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Order(0)
public class ControllerAdviceRest {

  @ExceptionHandler(AddressNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public List<ErrorMessage> exception(final AddressNotFoundException ex) {
    return Collections.singletonList(ErrorMessage.builder()
                                         .developerMessage("NotFound - Address Not Found")
                                         .userMessage("Address is not found in search by zipcode")
                                         .errorCode(20023)
                                         .build());
  }

  @ExceptionHandler(InvalidZipcodeException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public List<ErrorMessage> exception(final InvalidZipcodeException ex) {
    return Collections.singletonList(ErrorMessage.builder()
                                         .developerMessage("Zipcode is not valid")
                                         .userMessage("Zipcode is not valid")
                                         .errorCode(20042)
                                         .build());
  }

}
