package com.zipcode.finder.config;

import com.zipcode.finder.util.SecurityConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

  private final String jwtSecretKey;

  public JwtAuthenticationFilter(final AuthenticationManager authenticationManager,
                                 final String jwtSecretKey) {
    super(authenticationManager);

    this.jwtSecretKey = jwtSecretKey;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain chain)
      throws ServletException, IOException {

    String header = request.getHeader(SecurityConstants.HEADER_STRING);

    if (header != null && header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
      String token = header.replace(SecurityConstants.TOKEN_PREFIX, "");

      try {
        final Claims claims = Jwts.parser()
                                .setSigningKey(jwtSecretKey
                                                   .getBytes(StandardCharsets.UTF_8.displayName()))
                                .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX,
                                    ""))
                                .getBody();

        if (!Objects.isNull(claims)) {
          UsernamePasswordAuthenticationToken auth =
              new UsernamePasswordAuthenticationToken(claims, null,
              Collections.singletonList(new SimpleGrantedAuthority("admin")));

          SecurityContextHolder.getContext().setAuthentication(auth);
        }

      } catch (Exception e) {
        SecurityContextHolder.clearContext();
      }
    }
    chain.doFilter(request, response);
  }

}
