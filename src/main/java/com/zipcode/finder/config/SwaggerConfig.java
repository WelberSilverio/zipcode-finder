package com.zipcode.finder.config;

import static springfox.documentation.builders.PathSelectors.regex;

import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Value("${info.build.version}")
  private String version;

  @Value("${info.build.name}")
  String appName;

  @Value("${info.build.description}")
  String appDescription;

  public static final String AUTHORIZATION_HEADER = "Authorization";
  public static final String DEFAULT_INCLUDE_PATTERN = "/v[0-9]+/.*";

  @Bean
  public Docket postsApi() {
    return new Docket(DocumentationType.SWAGGER_2).groupName("com.zipcode")
               .securityContexts(Lists.newArrayList(securityContext()))
               .securitySchemes(Lists.newArrayList(apiKey()))
               .apiInfo(apiInfo()).select()
               .paths(regex("/.*"))
               .apis(Predicates.not(RequestHandlerSelectors
                                        .basePackage("org.springframework.boot")))
               .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
               .title(appName)
               .description(appDescription)
               .version(version)
               .build();
  }

  private ApiKey apiKey() {
    return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder()
               .securityReferences(defaultAuth())
               .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
               .build();
  }

  List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope
        = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Lists.newArrayList(
        new SecurityReference("JWT", authorizationScopes));
  }
}

