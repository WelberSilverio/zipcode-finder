package com.zipcode.finder.mock;

import com.zipcode.finder.dto.Address;
import java.util.Arrays;
import java.util.List;

public class ListAddressMock {
  public static List<Address> getListAddress() {
    return Arrays.asList(
        Address.builder()
            .zipcode("11222456")
            .street("Rua Desconhecida")
            .district("Centro")
            .city("Lugar Nenhum")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("11222000")
            .street("Rua Qualquer")
            .district("Integração")
            .city("Tão Tão Distante")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("11333456")
            .street("Rua Sem Nome")
            .district("Vila Conceição")
            .city("Interior")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("11300000")
            .street("Beco Sem Saida")
            .district("Vila Benedita")
            .city("Cidade Sem Nome")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("15646513")
            .street("Avenida Paulista")
            .district("Centro")
            .city("Noite Clara")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("57500000")
            .street("Avenida Presidente Vargas")
            .district("Parque Brasil")
            .city("Noite Escura")
            .state("RJ")
            .build(),
        Address.builder()
            .zipcode("11000000")
            .street("Rua Perdida")
            .district("Bairro Qualquer")
            .city("Cidade Qualquer")
            .state("SP")
            .build(),
        Address.builder()
            .zipcode("89513212")
            .street("Rua Das Estrelas")
            .district("Parque Chacara")
            .city("Novo Mundo")
            .state("BA")
            .build(),
        Address.builder()
            .zipcode("84643123")
            .street("Rua Bonita")
            .district("Vila Formosa")
            .city("Paraiso")
            .state("PA")
            .build(),
        Address.builder()
            .zipcode("65432154")
            .street("Rua do Patrocinio")
            .district("Parque Distrito")
            .city("Cidade das Maquinas")
            .state("AL")
            .build(),
        Address.builder()
            .zipcode("46574895")
            .street("Rua Exclusiva")
            .district("Residencial Restrito")
            .city("Cidade Secreta")
            .state("AM")
            .build(),
        Address.builder()
            .zipcode("13216489")
            .street("Rua Ladrilhada")
            .district("Vila Rural")
            .city("Vilarejo Qualquer")
            .state("RS")
            .build(),
        Address.builder()
            .zipcode("46546512")
            .street("Rua do Gado")
            .district("Parque Fazenda")
            .city("Cidade do Boi")
            .state("GO")
            .build(),
        Address.builder()
            .zipcode("45654654")
            .street("Rua dos Bandeirantes")
            .district("Loteamento Novo")
            .city("Palmas")
            .state("TO")
            .build()
    );
  }
}
