package com.zipcode.finder.v1.controller;

import com.zipcode.finder.dto.Address;
import com.zipcode.finder.dto.ResponseMeta;
import com.zipcode.finder.exception.InvalidZipcodeException;
import com.zipcode.finder.util.Constants;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseController {

  @Value("${info.build.version}")
  protected String applicationVersion;

  public String normalizeZipcode(String zipcode) {
    zipcode = zipcode.replaceAll("/\\D/g", zipcode);
    if (!Constants.ZIPCODE_LENGTH.equals(zipcode.length())
        || zipcode.equals(Constants.INVALID_ZIPCODE)) {
      throw new InvalidZipcodeException("Zipcode is not Valid");
    }
    return zipcode;
  }

  public ResponseEntity<ResponseMeta> buildResponse(HttpStatus status, Address address) {
    return ResponseEntity.status(status).body(
        ResponseMeta.builder()
            .meta(
                ResponseMeta.Meta.builder()
                    .version(this.applicationVersion)
                    .build())
            .records(Collections.singletonList(address))
            .build());
  }
}
