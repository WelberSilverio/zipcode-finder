package com.zipcode.finder.v1.controller;

import com.zipcode.finder.business.FinderBusiness;
import com.zipcode.finder.dto.ResponseMeta;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class FinderController extends BaseController {

  private final FinderBusiness finderBusiness;

  @Autowired
  public FinderController(final FinderBusiness finderBusiness) {
    this.finderBusiness = finderBusiness;
  }

  @ApiOperation("Find Address by zipcode")
  @ApiResponses(
      value = {
          @ApiResponse(code = 200, message = "Address found"),
          @ApiResponse(code = 400, message = "Parameters invalid"),
          @ApiResponse(code = 404, message = "Entity not found"),
          @ApiResponse(code = 500, message = "Internal server error")
      })
  @GetMapping(value = "/zipcodes/{zipcode}")
  @ResponseBody
  public ResponseEntity<ResponseMeta> findZipcode(
      @PathVariable("zipcode") String zipcode) {
    return this.buildResponse(HttpStatus.OK, finderBusiness
                                        .findAddress(this.normalizeZipcode(zipcode)));
  }

}
