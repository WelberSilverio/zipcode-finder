package com.zipcode.finder.util;

public class Constants {
  public static final Integer ZIPCODE_LENGTH = 8;
  public static final String INVALID_ZIPCODE = "00000000";
}
