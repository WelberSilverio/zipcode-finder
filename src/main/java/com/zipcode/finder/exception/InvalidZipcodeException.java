package com.zipcode.finder.exception;

public class InvalidZipcodeException extends RuntimeException {
  private static final long serialVersionUID = -567754143736652314L;

  public InvalidZipcodeException(String message) {
    super(message);
  }
}
