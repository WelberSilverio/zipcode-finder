package com.zipcode.finder.exception;

public class AddressNotFoundException extends RuntimeException {
  private static final long serialVersionUID = 478760948044031838L;

  public AddressNotFoundException(String message) {
    super(message);
  }
}
