package com.zipcode.finder;

import com.zipcode.finder.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfig.class)
@EnableCaching
public class ZipcodeFinderApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZipcodeFinderApplication.class, args);
  }

}
