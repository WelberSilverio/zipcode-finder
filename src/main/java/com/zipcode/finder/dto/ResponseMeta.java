package com.zipcode.finder.dto;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@AllArgsConstructor
public class ResponseMeta implements Serializable {

  private static final long serialVersionUID = -8355923733892696582L;
  private final ResponseMeta.Meta meta;
  private final List<? extends Serializable> records;

  public String toString() {
    ResponseMeta.Meta var10000 = this.getMeta();
    return "ResponseMeta(meta=" + var10000 + ", records=" + this.getRecords() + ")";
  }

  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  @Setter
  @Builder
  public static class Meta implements Serializable {
    private static final long serialVersionUID = -3992980364349941426L;
    private String version;

    public String toString() {
      String var10000 = this.getVersion();
      return "ResponseMeta.Meta(version=" + var10000 + ")";
    }
  }
}
