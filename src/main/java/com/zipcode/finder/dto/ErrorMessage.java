package com.zipcode.finder.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage implements Serializable {
  private static final long serialVersionUID = 4329941295478194253L;
  private String developerMessage;
  private String userMessage;
  private int errorCode;
}
