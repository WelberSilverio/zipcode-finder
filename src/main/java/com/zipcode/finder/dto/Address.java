package com.zipcode.finder.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address implements Serializable {

  private static final long serialVersionUID = 9047627627056904097L;

  private String zipcode;

  private String street;

  private String city;

  private String district;

  private String state;
}
