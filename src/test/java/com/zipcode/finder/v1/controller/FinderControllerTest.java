package com.zipcode.finder.v1.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.zipcode.finder.business.impl.FinderBusinessImpl;
import com.zipcode.finder.config.ControllerAdviceRest;
import com.zipcode.finder.dto.ResponseMeta;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(JUnitPlatform.class)
@AutoConfigureMockMvc
public class FinderControllerTest {

  private FinderController finderController;

  private MockMvc mockMvc;

  @BeforeEach
  public void setUp() {
    this.finderController = new FinderController(new FinderBusinessImpl());
    this.mockMvc = MockMvcBuilders.standaloneSetup(finderController)
                       .setControllerAdvice(new ControllerAdviceRest()).build();
  }

  @Test
  @SneakyThrows
  public void testConsultSuccess(){
    this.mockMvc.perform(
        MockMvcRequestBuilders.get("/v1/zipcodes/11222456"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  @SneakyThrows
  public void testConsultSuccessWithReplaceDigit(){
    this.mockMvc.perform(
        MockMvcRequestBuilders.get("/v1/zipcodes/11222457"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.records[0].zipcode")
                       .value("11222000"));
  }

  @Test
  @SneakyThrows
  public void testConsultInvalidSizeZipcode(){
    this.mockMvc.perform(
        MockMvcRequestBuilders.get("/v1/zipcodes/11222"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void testConsultInvalidZipcodeValue(){
    this.mockMvc.perform(
        MockMvcRequestBuilders.get("/v1/zipcodes/00000000"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void testConsultNotFoundAddress(){
    this.mockMvc.perform(
        MockMvcRequestBuilders.get("/v1/zipcodes/77777777"))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }
}