# Zipcode Finder
> Projeto responsavel pela busca de endereços por CEP.

## Requisitos
```sh
Java 11
Plugin Lombok
```

## Instalação OS X & Linux:

**Lombok plugin:**

```sh
Intellij: https://projectlombok.org/setup/intellij
Eclipse : https://projectlombok.org/setup/eclipse
```
## Ambiente
Configure as variáveis de ambiente de acordo com o ambiente `src/main/application.yml`:

Nome           | Descrição           | Valor Padrão  
---------------|---------------------|--------------
`ENV`      | Ambiente em que o app está rodando (development, staging ou production)                 | development        |  
`PORT`       | Porta a ser utilizada pela aplicação quando levantada          | 5001        | 
`JWT_TOKEN`     | Chave de autenticação para Token JWT       | test        | 

**Acessando as variáveis:**

```
  @Value("${api.url}")
  private String baseUrl;
```

## Configuração para Desenvolvimento

Acessar a pasta raiz do projeto

**Compilar o projeto:**

```sh
sdk use java 11.0.2-open
./mvnw clean package
```

**Executar o coverage:**

```sh
sdk use java 11.0.2-open
./mvnw clean install jacoco:report
```

**Executar o projeto com configuração local:**

```sh
sdk use java 11.0.2-open
./mvnw clean spring-boot:run -Dspring-boot.run.profiles=local
```

**Consultar documentação via Swagger:**

Após rodar a aplicação, acessar em seu navegador:
```sh
seu_host + "/swagger-ui.html"
```
## Dados de endereços Mockados

zipcode  | street | district | city | state
---------|--------|----------|------|-------
11222456 | Rua Desconhecida | Centro | Lugar Nenhum | SP
11222000 | Rua Qualquer | Integração | Tão Tão Distante | SP
11333456 | Rua Sem Nome | Vila Conceição | Interior | SP
11300000 | Beco Sem Saida | Vila Benedita | Cidade Sem Nome | SP
15646513 | Avenida Paulista | Centro | Noite Clara | SP
57500000 | Avenida Presidente Vargas | Parque Brasil | Noite Escura | RJ
11000000 | Rua Perdida | Bairro Qualquer | Cidade Qualquer | SP
89513212 | Rua Das Estrelas | Parque Chacara | Novo Mundo | BA
84643123 | Rua Bonita | Vila Formosa | Paraiso | PA
65432154 | Rua do Patrocinio | Parque Distrito | Cidade das Maquinas | AL
46574895 | Rua Exclusiva | Residencial Restrito | Cidade Secreta | AM
13216489 | Rua Ladrilhada | Vila Rural | Vilarejo Qualquer | RS
46546512 | Rua do Gado | Parque Fazenda | Cidade do Boi | GO
45654654 | Rua dos Bandeirantes | Loteamento Novo | Palmas | TO

# Visão geral
Um Código de Endereçamento Postal (CEP) no Brasil é composto por 8 algarismos e utilizado para localizar determinado endereço, uma vez que seu código está relacionado a um lugar especifico podendo ser uma rua, uma região, um bairro específico ou até mesmo o prédio.


## Contexto
Afim de ajudar em preenchimentos de formulários de cadastro de endereços, surgiu a necessidade de uma aplicação que provesse a rua, o bairro,a cidade e o estado de determinado CEP, melhorando a experiência do usuário ao otmizar o seu tempo.


## Objetivos
1. Criar uma base dados contendo o CEP e os dados referentes ao seu endereço (RUA, BAIRRO, CIDADE e ESTADO)
2. Retornar os dados de endereço vinculados a um CEP válido após pesquisa em uma aplicação


## Fora de Escopo
Busca por CEP inválidos (Ex: 00000-000) ou não cadastrados.

## Solução Proposta
1. Criar uma lista de endereços contendo o CEP, RUA, BAIRRO, CIDADE e ESTADO.
2. Criar uma rota `GET` para obter os dados de endereço ao informar determinado CEP:
   1. Dado um CEP válido, deve retornar o endereço correspondente
   2. Dado um CEP válido, que não exista o endereço, deve substituir um dígito da direita para a esquerda por zero até que o endereço seja localizado
   3. Dado um CEP inválido, deve retornar uma mensagem reportando o erro

## Arquitetura
Foi criada uma API (em Java com SpringBoot) utilizando uma arquitetura em camadas.
A camada `Controller` é a responsável por receber as requisições HTTP, verificar os respectivos dados de entrada e montar os dados de saida da aplicação.
A `Business` é responsável por toda a gestão de regras de negócio da aplicação. Existem também classes de utilidade responsáveis pela segurança, tratativas de erros e outros serviços da aplicação.

A API segue o padrão REST, estabelecendo assim uma comunicação cliente/servidor utilizando protocolo HTTP e tendo como padrão de mensagem um objeto JSON.

## Fluxo
![fluxo](./images/fluxo.png)

### Payloads
#### Request
```sh
GET '/v1/zipcodes/{zipcode}'
```
#### Responses
Success - Status 200
```json
{
    "meta": {
        "version": "1.0.0"
    },
    "records": [
        {
            "zipcode": "11222000",
            "street": "Rua Qualquer",
            "district": "Integração",
            "city": "Tão Tão Distante",
            "state": "SP"
        }
    ]
}
```
Campo          | Descrição           | Tipo
---------------|---------------------|--------------
`zipcode`      | CEP                 | Texto
`street`       | Logradouro          | Texto
`district`     | Bairro              | Texto
`city`         | Cidade              | Texto
`state`        | Estado              | Texto 


Bad Request - Status 400
```json
[
   {
      "developerMessage": "Zipcode is not valid",
      "userMessage": "Zipcode is not valid",
      "errorCode": 20042
   }
]
```
Unauthorized - Status 401
```json
[
   {
      "developerMessage": "Unauthorized - Full authentication is required to access this resource",
      "userMessage": "You are not authorized to perform this operation",
      "errorCode": 30001
   }
]
```
Not Found - Status 404
```json
[
   {
      "developerMessage": "NotFound - Address Not Found",
      "userMessage": "Address is not found in search by zipcode",
      "errorCode": 20023
   }
]
```


## Testabilidade
* Testes unitários
* Testes de fluxo completo

## Segurança
A aplicação possui autenticação via token JWT, utilizando um Bearer Token configurável. O valor padrão, caso não definido, é `test`.

# [Questão 2](QUESTION2.md)