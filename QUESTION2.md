# Questão 2
### Quando você digita a URL de um site (http://www.netshoes.com.br) no browser e pressiona enter, explique da forma que preferir, o que ocorre nesse processo do protocolo HTTP entre o Client e o Server.

Um protocólo HTTP é utilizado para a comunicação entre um cliente e servidor.
O cliente manda uma solicitação de texto para um servidor, que por sua vez devolve uma resposta ao cliente.
Ao devolver a resposta é cortada a comunicação entre cliente e servidor.

Utilizam-se de verbos na requisição para informar o tipo de operação a ser efetuada pelo servidor:
* **GET** : utilizado para receber informações do servidor.
* **POST** : utilizado para informar dados, que na maioria das vezes será gravado no lado do servidor.
* **DELETE** : utilizado para remover informações armazenadas no lado do servidor.
* **PUT** : utilizado para alterar informações armazenadas no lado do servidor, com obrigatoriedade de preenchimento de todos atributos do serviço requerido.
* **PATCH** : utilizado para alterar informações armazenadas no lado do servidor, mas sem a obrigatoriedade de preenchimento de todos atributos do serviço requerido.

Existem por convenção status de resposta para todas as requisições HTTP. 
Os status da faixa de 200 represemtam sucesso na requisição. 
Já os da faixa de 400  representam erros da parte do Cliente, como por exemplo a busca de informação inexistente (Status 404), falta de autenticação (Status 401) e requisição com sintaxe invalida (Status 400).

Exemplificando com uma requisição a URL https://www.netshoes.com.br/ , o navegador busca este dominio e se conecta ao servidor passando uma requisição de tipo GET.
Caso não seja encontrada a pagina requerida pela URL informada, será retornado um status 404 de pagina não encontrada pelo servidor e a conexão finalizada, não renderizando assim o site.
Mas se a URL for encontrada, retornará um status de sucesso e o navegador começará o processo de renderização.
O navegador busca elementos que serão renderizados como  imagens, dados e informações adicionais, onde para cada um desses artefatos é feita uma nova requisição HTTP ao servidor.
Após carregar todos os elementos o navegador exibe a página solicitada pela URL informada.